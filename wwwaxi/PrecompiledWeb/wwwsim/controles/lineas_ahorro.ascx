﻿<%@ control language="vb" autoeventwireup="false" inherits="lineas_ahorro, App_Web_pw1okmjx" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="asp" %>

<head runat="server">
    <title>Lineas de Ahorro</title>
    <style type="text/css">
        .style1
        {
            width: 153px;
        }
        .style2
        {
            width: 298px;
        }
        .style3
        {
            width: 84%;
        }
    </style>
</head>
    <div>
			<asp:label id="Label10" Width="224px" Height="15px" Font-Size="Medium" Font-Names="Verdana"
				runat="server" ForeColor="#16387C" Font-Bold="True">LINEAS DE AHORRO</asp:label>
    </div>
    
    <table border="1" cellpadding="0" cellspacing="0" class="style3" 
    style="border-color: #003366; height: 364px;">
        <tr>
            <td>
        <table style="width: 74%;" align="center">
            <tr>
                <td class="style1">
                    
                    <asp:Label ID="Label1" runat="server" Text="Moneda" Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    
                    <asp:DropDownList ID="moneda_DropDownList" runat="server" Width="150px" 
                        Font-Names="Calibri">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    
                    <asp:Label ID="Label2" runat="server" Text="Capitalizacion" 
                        Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                   <asp:DropDownList ID="capitalizacion_DropDownList" runat="server" Width="150px" 
                        Font-Names="Calibri">
                    </asp:DropDownList>
                &nbsp;</td>
                <td>
                    &nbsp;
                    <asp:Label ID="mensaje" runat="server" Font-Names="Verdana" Font-Size="10pt" 
                        Text="No existe!!!" Font-Bold="True" ForeColor="Red" 
                        Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style1">
                    
                    <asp:Label ID="Label3" runat="server" Text="Tipo de Ahorro" 
                        Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    
                    <asp:DropDownList ID="cartera_DropDownList" runat="server" Width="230px" 
                        AutoPostBack="True" Font-Names="Calibri">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label4" runat="server" Text="Inst." Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    <asp:DropDownList ID="fondos_DropDownList" runat="server" Width="230px" 
                        Font-Names="Calibri">
                    </asp:DropDownList>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label5" runat="server" Text="Tasa" Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    <asp:TextBox ID="tasa_TextBox" runat="server" Width="60px" AutoPostBack="True" 
                        Font-Names="Calibri">12.00</asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label8" runat="server" Text="Dias plazo" Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    <asp:TextBox ID="plazo_TextBox" runat="server" Width="60px" AutoPostBack="True" 
                        Font-Names="Calibri">30</asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label9" runat="server" Text="Nombre linea" Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    <asp:TextBox ID="nombre_linea_TextBox" runat="server" Width="300px" 
                        Font-Names="Calibri"></asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label6" runat="server" Text="Presupuesto inferior" 
                        Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    <asp:TextBox ID="inferior_TextBox" runat="server" Font-Names="Calibri">0.00</asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    <asp:Label ID="Label7" runat="server" Text="Presupuesto superior" 
                        Font-Names="Calibri"></asp:Label>
                </td>
                <td class="style2">
                    <asp:TextBox ID="superior_TextBox" runat="server" Font-Names="Calibri">999999.99</asp:TextBox>
                </td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td class="style2">
                    &nbsp;</td>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td class="style1">
                    &nbsp;</td>
                <td class="style2">
                    <asp:Button ID="crear_Button" runat="server" BackColor="#99CC00" 
                        Font-Bold="False" Text="Crear Linea" Font-Names="Calibri" 
                        Font-Size="12pt" />
                </td>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
            </td>
        </tr>
</table>


<script language='JavaScript'>
document.onkeydown=function (evt) {return (evt ? evt.which : event.keyCode) != 13;}
</script>
<p>
&nbsp;&nbsp;&nbsp;
</p>

