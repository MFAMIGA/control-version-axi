<%@ control language="vb" autoeventwireup="false" inherits="Login, App_Web_tbtnmcmr" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<style type="text/css">
    .style4
    {
        width: 61%;
        height: 321px;
    }
    .style5
    {
        width: 136px;
    }
    .style6
    {
        width: 101%;
        height: 265px;
    }
    .style7
    {
        width: 504px;
    }
    .style8
    {
        width: 485px;
    }
    .style9
    {
        width: 100%;
        height: 4px;
    }
    .style2
    {
        width: 103%;
        height: 82px;
    }
    .style3
    {
        width: 94%;
        height: 3px;
    }
    .style10
    {
        width: 65%;
        height: 25px;
    }
    .style12
    {
        width: 65%;
        height: 27px;
    }
    .style13
    {
        height: 27px;
    }
    .style14
    {
        width: 131px;
    }
    .style15
    {
        width: 485px;
        height: 4px;
    }
    .style16
    {
        height: 25px;
    }
    .ToUpper
    {
    text-transform: uppercase;
    }	
</style>
<table class="style4">
    <tr>
        <td class="style5" bgcolor="#FFCC00">
            <table class="style6" bgcolor="#FFCC00">
                <tr>
                    <td>
			<TABLE style="WIDTH: 100px; HEIGHT: 168px" borderColor="#009900" bgColor="#ffffff" borderColorLight="#006600"
				border="1">
				<TR>
					<TD style="FONT-WEIGHT: bold; FONT-SIZE: 0.9em; COLOR: white; FONT-STYLE: normal; HEIGHT: 39px; BACKGROUND-COLOR: #5d7b9d"
						align="center" bgColor="#ffff66" colSpan="2"><asp:image id="Image1" Width="220px" ImageUrl="../imagenes/cabecera.jpg" runat="server" Height="46px"></asp:image></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 0.8em; WIDTH: 105px; HEIGHT: 17px" align="right" 
                        bgColor="Red">
                        <asp:label id="lblUsuario" Runat="server" Font-Names="Gill Sans MT" 
                            Font-Size="12pt">Usuario:</asp:label></TD>
					<TD style="HEIGHT: 17px" align="left" bgColor="Red">
                        <asp:textbox id="txtUsuario" Width="84px" Runat="server" 
                            Font-Names="Gill Sans MT" BorderColor="Black"
							Font-Size="12pt" CssClass="ToUpper" MaxLength="10"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 0.8em; WIDTH: 105px; HEIGHT: 15px" align="right" 
                        bgColor="Red">
                        <asp:label id="lblClave" Runat="server" Font-Names="Gill Sans MT" 
                            Font-Size="12pt">Clave:</asp:label></TD>
					<TD style="HEIGHT: 15px" align="left" bgColor="Red">
                        <asp:textbox id="txtClave" 
                            Width="83px" Runat="server" Font-Names="Gill Sans MT" BorderColor="Black"
							Font-Size="12pt" TextMode="Password"></asp:textbox></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 0.8em; WIDTH: 105px; HEIGHT: 15px" align="right" 
                        bgColor="Red">
                        <asp:label id="lblClave0" Runat="server" Font-Names="Gill Sans MT">Empresa:</asp:label></TD>
					<TD style="HEIGHT: 15px" align="left" bgColor="Red">
                        <asp:DropDownList ID="cbxfondos" runat="server" AutoPostBack="True" 
                            Font-Names="Century Gothic" Font-Size="8pt" Height="16px" Width="100px">
                        </asp:DropDownList>
                    </TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 0.8em; WIDTH: 105px; HEIGHT: 15px" align="right" 
                        bgColor="Red">
                        <asp:label id="lblClave1" Runat="server" Font-Names="Gill Sans MT" 
                            Visible="False">A�o:</asp:label></TD>
					<TD style="HEIGHT: 15px" align="left" bgColor="Red">
                        <asp:DropDownList ID="cbxanos" runat="server" 
                            Font-Names="Century Gothic" Font-Size="8pt" Height="17px" Width="99px" 
                            Enabled="False" Visible="False">
                        </asp:DropDownList>
                    </TD>
				</TR>
				<TR>
					<TD style="HEIGHT: 27px" borderColor="#3300cc" align="center" bgColor="Red" 
                        colSpan="2"><asp:button id="btnIn" runat="server" Font-Names="Gill Sans MT" Text="Ingresar"></asp:button></TD>
				</TR>
				<TR>
					<TD style="FONT-SIZE: 0.8em; COLOR: red" align="center" bgColor="#ffffff" colSpan="2"><asp:literal id="FailureText" Runat="server" EnableViewState="False" ></asp:literal></TD>
				</TR>
			</TABLE>
			        </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#FFCC00">
                        <asp:label id="Label2" 
                            Width="223px" runat="server" Height="27px" Font-Names="Gill Sans MT"
							Font-Size="9pt" ForeColor="Yellow" Font-Bold="True" BackColor="#FFCC00">AXI</asp:label>
						</td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#FFCC00">
                                    <asp:label id="Label3" Width="227px" 
                                        runat="server" Height="48px" Font-Names="Gill Sans MT"
										Font-Size="8pt" ForeColor="Yellow" Font-Bold="True" BackColor="#FFCC00"></asp:label>
                                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#FFCC00">
                                    <asp:label id="Label4" Width="195px" runat="server" 
                            Font-Names="Gill Sans MT" Font-Size="6pt"
										ForeColor="#FFFF66" Font-Bold="True" BackColor="#FFCC00"></asp:label>
                                    </td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#FFCC00">
						<asp:hyperlink id="HyperLink1" runat="server" Height="24px" 
                            ForeColor="#FF6600" NavigateUrl="http://www.axi.com"
							Font-Names="Gill Sans MT" Font-Size="8pt" BackColor="#FFCC00" Width="224px">http://www.axi.com/</asp:hyperlink>
					</td>
                </tr>
                <tr>
                    <td align="center" bgcolor="#FFCC00">
                        <asp:label id="Label12" 
                            Width="223px" runat="server" Height="27px" Font-Names="Gill Sans MT"
							Font-Size="9pt" ForeColor="#FF6600" Font-Bold="False" BackColor="#FFCC00">Ver.11082014</asp:label>
					</td>
                </tr>
            </table>
        </td>
        <td align="justify" class="style7">
            <table class="style6">
                <tr>
                    <td align="justify" class="style8">
                        <asp:label id="lblIntro1" Width="492px" runat="server" 
                            Font-Names="Gill Sans MT" Font-Size="40pt"
							Height="120px">AXI</asp:label>
                    </td>
                </tr>
                <tr>
                    <td align="justify" class="style8">
			<asp:label id="Label6" Height="40px" runat="server" Width="494px" Font-Names="Gill Sans MT"
				Font-Size="10pt">MISI�N. Ofrecemos servicios financieros responsables orientados en 
                        mejorar la calidad de vida de nuestros clientes.</asp:label>
                    </td>
                </tr>
                <tr>
                    <td align="justify" class="style8">
			<asp:label id="Label10" Height="40px" runat="server" Width="494px" Font-Names="Gill Sans MT"
				Font-Size="10pt"> Ser l�der en servicios financieros a trav�s del compromiso social, 
                        para el desarrollo familiar, por un mejor M�xico.</asp:label>
		            </td>
                </tr>
                <tr>
                    <td align="justify" class="style8">
			<asp:label id="Label13" Height="40px" runat="server" Width="494px" Font-Names="Gill Sans MT"
				Font-Size="10pt">VALORES. 1.- Compromiso Social Es trabajar cotidianamente poniendo ese �plus� extra para hacer de nuestra sociedad una zona de libre armon�a y sana convivencia. 2. Honestidad Es una cualidad humana por lo que la persona se determina elegir actuar siempre en base en la verdad y en la aut�ntica justicia. 3.- Integridad Honradez y rectitud en la conducta Es hacer lo correcto, por las razones correctas, del modo correcto 4.- Lealtad Fidelidad al compromiso de defender lo que creemos y en quien creemos, en los buenos y en los malos momentos. 5.- Respeto Significa valorar a los dem�s, a acatar su autoridad y considerar su dignidad.</asp:label>
			    </td>
                </tr>
                <tr>
                    <td align="justify" class="style8">
			<asp:label id="Label11" Height="40px" runat="server" Width="494px" Font-Names="Gill Sans MT"
				Font-Size="10pt"></asp:label>
		            </td>
                </tr>
            </table>
                        <table cellpadding="0" cellspacing="0" class="style2" 
                align="center">
                            <tr>
                                <td align="center" bgcolor="#CCCC00">
                                    <asp:Label ID="Label1" runat="server" BackColor="#CCCC00" Font-Names="Gill Sans MT" 
                                        ForeColor="White" Text="Cambiar la contrase�a"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" cellpadding="0" cellspacing="0" class="style3">
                                        <tr>
                                            <td align="right" class="style15">
                                                <asp:Label ID="Label7" runat="server" Font-Names="Gill Sans MT" 
                                                    Text="Contrase�a Actual:" Font-Size="9pt"></asp:Label>
                                            </td>
                                            <td class="style9">
                                                <asp:textbox id="txtClave2" 
                            Width="100px" Runat="server" Font-Names="Gill Sans MT" BorderColor="Black"
							Font-Size="0.8em" TextMode="Password" Height="21px"></asp:textbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="style10" bgcolor="White">
                                                <asp:Label ID="Label8" runat="server" Font-Names="Gill Sans MT" 
                                                    Text="Nueva Contrase�a:" Font-Size="9pt"></asp:Label>
                                            </td>
                                            <td class="style16">
                                                <asp:textbox id="txtClave0" 
                            Width="100px" Runat="server" Font-Names="Gill Sans MT" BorderColor="Black"
							Font-Size="0.8em" TextMode="Password" Height="21px"></asp:textbox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="right" class="style12">
                                                <asp:Label ID="Label9" runat="server" Font-Names="Gill Sans MT" 
                                                    Text="Confirmar Nueva Contrase�a:" Font-Size="9pt"></asp:Label>
                                            </td>
                                            <td class="style13">
                                                <asp:textbox id="txtClave1" 
                            Width="100px" Runat="server" Font-Names="Gill Sans MT" BorderColor="Black"
							Font-Size="0.8em" TextMode="Password" Height="21px"></asp:textbox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:Label ID="Label5" runat="server" Font-Names="Gill Sans MT" ForeColor="Red" 
                                        
                                        Text="Confirmar la nueva contrase�a debe coincidir con la entrada Nueva contrase�a." 
                                        Font-Size="9pt"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table align="center" cellpadding="0" cellspacing="0" class="style5">
                                        <tr>
                                            <td align="center" class="style14">
                                                <asp:Button ID="Button1" runat="server" Font-Names="Gill Sans MT" Text="Confirmar" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
        </td>
    </tr>
</table>

